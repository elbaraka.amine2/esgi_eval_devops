FROM ubuntu:16.04


WORKDIR /usr/src/app

COPY . /usr/src/app

RUN addgroup -g  1 -S esgi
RUN apt-get update && apt-get install -y --no-install-recommends \
    curl=7.52.1-5+deb9u12 \
    nginx=1.10.3-1+deb9u5 \
    python-pip=20.3\
    python=3.7\
    git=1:2.11.0-3+deb9u7 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN adduser -u 2 -S jordan -G 2
RUN adduser -u 2 -S jonathan -G 2
Expose 80
CMD ["sleep", "infinity"]



# CMD ["python", "server.py"]